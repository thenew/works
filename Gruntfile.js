var assets_path = 'app/assets';

module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    stylus: {
      compile: {
        files: {
          'app/assets/css/main.css': 'app/assets/styles/main.styl'
        }
      }
    },
    autoprefixer:{
      options: {
        map: true,
        browsers: ['last 2 versions', 'ie 9']
      },
      dist:{
        expand: true,
        flatten: true,
        cwd: assets_path+'/css/',
        src: ['*.css'],
        dest: assets_path+'/css/'
      }
    },
    concat: {
      dist: {
        src: [
          assets_path+'/js/jquery-2.2.2.min.js',
          assets_path+'/js/TweenMax.min.js',
          assets_path+'/js/DrawSVGPlugin.min.js',
        ],
        dest: assets_path+'/dist/js/vendor.js',
      },
    },
    uglify: {
      build: {
        src: assets_path+'/dist/js/vendor.js',
        dest: assets_path+'/dist/js/vendor.js'
      },
      main: {
        src: assets_path+'/js/main.js',
        dest: assets_path+'/dist/js/main.js'
      }
    },
    watch: {
      js: {
          files: [assets_path+'/js/**/*.js'],
          tasks: ['js'],
          options: {
              spawn: false,
          }
      },
      css: {
        files: assets_path+'/styles/**/*.styl',
        tasks: ['styles']
      }
    }

  });

  grunt.registerTask('styles', ['stylus', 'autoprefixer']);
  grunt.registerTask('js', ['concat', 'uglify']);
  // grunt.registerTask('default', ['concat', 'uglify']);
}

