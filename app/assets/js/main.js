isXs = window.matchMedia( "(max-width: 768px)" ).matches;
isSmDown = window.matchMedia( "(max-width: 992px)" ).matches;

// Minified version of isMobile included in the HTML since it's small
!function(a){var b=/iPhone/i,c=/iPod/i,d=/iPad/i,e=/(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,f=/Android/i,g=/(?=.*\bAndroid\b)(?=.*\bSD4930UR\b)/i,h=/(?=.*\bAndroid\b)(?=.*\b(?:KFOT|KFTT|KFJWI|KFJWA|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|KFARWI|KFASWI|KFSAWI|KFSAWA)\b)/i,i=/IEMobile/i,j=/(?=.*\bWindows\b)(?=.*\bARM\b)/i,k=/BlackBerry/i,l=/BB10/i,m=/Opera Mini/i,n=/(CriOS|Chrome)(?=.*\bMobile\b)/i,o=/(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,p=new RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)","i"),q=function(a,b){return a.test(b)},r=function(a){var r=a||navigator.userAgent,s=r.split("[FBAN");return"undefined"!=typeof s[1]&&(r=s[0]),s=r.split("Twitter"),"undefined"!=typeof s[1]&&(r=s[0]),this.apple={phone:q(b,r),ipod:q(c,r),tablet:!q(b,r)&&q(d,r),device:q(b,r)||q(c,r)||q(d,r)},this.amazon={phone:q(g,r),tablet:!q(g,r)&&q(h,r),device:q(g,r)||q(h,r)},this.android={phone:q(g,r)||q(e,r),tablet:!q(g,r)&&!q(e,r)&&(q(h,r)||q(f,r)),device:q(g,r)||q(h,r)||q(e,r)||q(f,r)},this.windows={phone:q(i,r),tablet:q(j,r),device:q(i,r)||q(j,r)},this.other={blackberry:q(k,r),blackberry10:q(l,r),opera:q(m,r),firefox:q(o,r),chrome:q(n,r),device:q(k,r)||q(l,r)||q(m,r)||q(o,r)||q(n,r)},this.seven_inch=q(p,r),this.any=this.apple.device||this.android.device||this.windows.device||this.other.device||this.seven_inch,this.phone=this.apple.phone||this.android.phone||this.windows.phone,this.tablet=this.apple.tablet||this.android.tablet||this.windows.tablet,"undefined"==typeof window?this:void 0},s=function(){var a=new r;return a.Class=r,a};"undefined"!=typeof module&&module.exports&&"undefined"==typeof window?module.exports=r:"undefined"!=typeof module&&module.exports&&"undefined"!=typeof window?module.exports=s():"function"==typeof define&&define.amd?define("isMobile",[],a.isMobile=s()):a.isMobile=s()}(this);


var $window = $(window),
    $animation_elements = $('.work'),
    $bgGradient = $('.bg-gradient'),
    bgGradientInBetween = false;


$(document).ready(function() {
    $('html').removeClass('no-js').addClass('js');
    if(isMobile.any) {
        $('html').addClass('is-mobile');
    }

    // Touch Device Detection
    var isTouchDevice = 'ontouchstart' in document.documentElement;
    if( isTouchDevice ) {
        $('html').addClass('touch');
    }else {
        $('html').addClass('no-touch');
    }


    var testEl = document.createElement( "x-test" );
    var supportsWebkitBackgroundClipText = typeof testEl.style.webkitBackgroundClip !== "undefined" && ( testEl.style.webkitBackgroundClip = "text", testEl.style.webkitBackgroundClip === "text" );

    if (supportsWebkitBackgroundClipText) {
        document.documentElement.classList.add("backgroundcliptext");
    }

    if( ! isXs ) {
        var $videos = $('video[data-autoplay]');
        $videos.each(function(i, video) {
            var $video = $(video);
            $video.removeAttr('data-autoplay')
                  .attr('preload', 'false');

            if(i == 0) {
                $video.attr('autoplay', 'true');
            } else {
                // setTimeout(function() {
                    // $video.attr('autoplay', 'true');
                    // video.play();
                // }, 500);
            }

        });

    }


    // init

    // create bgs
    // $.each($animation_elements, function(i, el) {
    //     var bg = $('<div class="bg"></div>');

    //     if(i == 0) {
    //         bg.addClass('active')
    //             .css({'background-color': $(this).data('color') })
    //     }

    //     $('.bgs').append(bg);
    // });


    // on scroll
    $window.on('scroll resize', check_if_in_view);
    check_if_in_view();
    // $window.trigger('scroll');

    degradient();

    setTimeout(function() {
        $('html').addClass('js-ready');
        jQuery('.logo-svg').css({ 'opacity': 0 });
        logoAnim();
    }, 400);

});

$(window).load(function() {


    // fix vh
    fixVh();
    $window.on('resize', fixVh);

});


function degradient() {

    // init
    var mids = [];
    $animation_elements.each(function(i, el) {
        var $element = $(el);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var mid = element_top_position + (element_height/2);
        mids[i] = mid;
    });

    $(window).load(function() {

        $animation_elements.each(function(i, el) {
            var $element = $(el);
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var mid = element_top_position + (element_height/2);
            mids[i] = mid;
        });
    });


    var degradientScroll = function() {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var viewportMid = window_top_position + (window_height/2);
        var gradientString;

        $.each(mids, function(i, mid) {

            if(viewportMid >= mid) {

                var A = $animation_elements[i];
                var Amid = mids[i];
                var Acolor = $(A).data('color');
                var B = $animation_elements[i+1];
                var Bmid = mids[i+1];
                var Bcolor = $(B).data('color');
                var gap = Bmid - Amid;
                var neutralZone = 40; // percent
                var viewportMidInGap = (viewportMid - Amid);

                var percentVpInGap = Math.round( ( ( viewportMidInGap - ( ( (gap/100) * neutralZone ) / 2 ) ) / (gap - ( (gap/100) * neutralZone ) ) ) * 100 );

                // this will make $number 1 if it's lower than 1, and 20 if it's higher than 20
                percentVpInGap = Math.min(Math.max(parseInt(percentVpInGap), 0), 100);

                var x = 100;

                if(percentVpInGap < 25) {
                    x = 100 - (percentVpInGap*2);
                    var Balpha = percentVpInGap / 25;
                    Balpha = Balpha.toFixed(1);

                    gradientString = Acolor+' 0%,'+Acolor+' '+x+'%,'+hexToRgba(Bcolor, Balpha)+' 100%';
                }
                else if(percentVpInGap >= 25 && percentVpInGap <= 50) {
                    x = 100 - (percentVpInGap*2);

                    gradientString = Acolor+' 0%,'+Acolor+' '+x+'%,'+Bcolor+' 100%';
                }
                else if(percentVpInGap > 50 && percentVpInGap <= 75 ) {
                    x = 100 - ( (percentVpInGap-50) * 2 );

                    gradientString = Acolor+' 0%,'+Bcolor+' '+x+'%,'+Bcolor+' 100%';
                }
                else if(percentVpInGap > 75 ) {
                    x = 0;
                    x = 100 - ( (percentVpInGap-50) * 2 );
                    var Aalpha = ( ( (percentVpInGap - 75) / 25 ) - 1 ) * (-1);
                    Aalpha = Aalpha.toFixed(1);

                    gradientString = hexToRgba(Acolor, Aalpha)+' 0%,'+Bcolor+' '+x+'%,'+Bcolor+' 100%';
                }

            }

        });

        $bgGradient.css({
            'background': '-webkit-linear-gradient(top, '+gradientString+')',
            'background-image': 'linear-gradient(to bottom,'+gradientString+')'
        });

    }
    $window.on('scroll', degradientScroll);
    degradientScroll();

}

function hexToRgba(hex,alpha){
    hex = hex.toUpperCase();
    var h = "0123456789ABCDEF";
    var r = h.indexOf(hex[1])*16+h.indexOf(hex[2]);
    var g = h.indexOf(hex[3])*16+h.indexOf(hex[4]);
    var b = h.indexOf(hex[5])*16+h.indexOf(hex[6]);
    return "rgba("+r+", "+g+", "+b+", "+alpha+")";
}

function fixVh() {
    if(isMobile.any) {
        return;
    }

    $.each($('.works-list .media'), function(i, el) {
        $(el).css({
            height: 'auto'
        });
        $(el).css({
            height: $(el).height()
        });
    });
}

function logoAnim() {

    var
    logoSvg = jQuery('.logo-svg'),
    circle = jQuery('.circle'),
    t1 = jQuery('.t1'),
    t2 = jQuery('.t2'),
    n = jQuery('.n'),

    circleClone3 = circle.clone().insertBefore( circle ).css('stroke', '#FFFF22'), // 557290
    circleClone2 = circle.clone().insertBefore( circle ).css('stroke', '#5ECEBF'), // 64c49c
    circleClone1 = circle.clone().insertBefore( circle ).css('stroke', '#B39BE0'),

    t1Clone3 = t1.clone().insertBefore( t1 ).css('stroke', '#FFFF22').css('stroke-width', '4.4'),
    t1Clone2 = t1.clone().insertBefore( t1 ).css('stroke', '#5ECEBF').css('stroke-width', '4.4'),
    t1Clone1 = t1.clone().insertBefore( t1 ).css('stroke', '#B39BE0').css('stroke-width', '4.4'),

    t2Clone3 = t2.clone().insertBefore( t2 ).css('stroke', '#FFFF22'),
    t2Clone2 = t2.clone().insertBefore( t2 ).css('stroke', '#5ECEBF'),
    t2Clone1 = t2.clone().insertBefore( t2 ).css('stroke', '#B39BE0'),

    nClone3 = n.clone().insertBefore( n ).css('stroke', '#FFFF22').css('stroke-width', '4.4'),
    nClone2 = n.clone().insertBefore( n ).css('stroke', '#5ECEBF').css('stroke-width', '4.4'),
    nClone1 = n.clone().insertBefore( n ).css('stroke', '#B39BE0').css('stroke-width', '4.4'),


    logoTl = new TimelineMax({
      delay:0,
      repeat:0,
      repeatDelay: 1,
      paused: 0
    });

    logoTl
    .set([logoSvg], {
      rotation: '40deg',
      scale: 0.8,
      opacity: 1,
      ease:Power3.easeInOut
    })
    .set([circle, circleClone1, circleClone2, circleClone3], {
      opacity: 0,
      drawSVG:'0% 0%',
      scaleY: -1,
      rotation: '-120deg',
      transformOrigin: '50% 50%'
    })
    .set([t1, t1Clone1, t1Clone2, t1Clone3], {
      opacity: 0,
      drawSVG:'0% 0%',
      transformOrigin: '50% 50%'
    })
    .set([t2, t2Clone1, t2Clone2, t2Clone3], {
      opacity: 0,
      drawSVG:'0% 0%',
      transformOrigin: '50% 50%'
    })
    .set([n, nClone1, nClone2, nClone3], {
      opacity: 0,
      drawSVG:'100% 100%',
      transformOrigin: '50% 50%'
    })

    .add("start")

    .add("t1", "+=0.3")
    .add("t2", "+=0.1")
    .add("n", "+=0.1")

    .to([circle, circleClone1, circleClone2, circleClone3], 0.2, {
      opacity: 1,
      ease:Power3.easeInOut
    })
    .to(circleClone3, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.4")
    .to(circleClone2, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.9")
    .to(circleClone1, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.9")
    .to(circle, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.9")



    t1Tl = new TimelineMax({
      paused: 0
    });
    t1Tl
    .to([t1, t1Clone1, t1Clone2, t1Clone3], 0.2, {
      opacity: 1,
      ease:Power3.easeInOut
    }, "-=0.9")
    .to(t1Clone3, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.4")
    .to(t1Clone2, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.9")
    .to(t1Clone1, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.9")
    .to(t1, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.9")
    .to([t1Clone1, t1Clone2, t1Clone3], 0, {
        opacity: 0
    })


    t2Tl = new TimelineMax({
      paused: 0
    });
    t2Tl
    .to([t2, t2Clone1, t2Clone2, t2Clone3], 0.2, {
      opacity: 1,
      ease:Power3.easeInOut
    }, "-=1.5")
    .to(t2Clone3, 0.5, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.4")
    .to(t2Clone2, 0.5, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.4")
    .to(t2Clone1, 0.5, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.4")
    .to(t2, 0.5, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.4")


    nTl = new TimelineMax({
      paused: 0
    });
    nTl
    .to([n, nClone1, nClone2, nClone3], 0.2, {
      opacity: 1,
      ease:Power3.easeInOut
    }, "-=1.5")
    .to(nClone3, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.4")
    .to(nClone2, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.9")
    .to(nClone1, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.9")
    .to(n, 1, {
      drawSVG:'0% 100%',
      ease:Power3.easeInOut
    }, "-=0.9")

    svgTl = new TimelineMax({
      paused: 0
    });
    svgTl
    .to(logoSvg, 1.5, {
      rotation:'0deg',
      ease:Elastic.easeOut
    })

    svgScaleTl = new TimelineMax({
      paused: 0
    });
    svgScaleTl
    .to(logoSvg, 1, {
      scale:1,
      ease:Back.easeOut
    })


    logoTl.add(svgTl, "start");
    logoTl.add(svgScaleTl, "start");
    logoTl.add(t1Tl, "t1");
    logoTl.add(t2Tl, "t2");
    logoTl.add(nTl, "n");

    logoTl.timeScale(1)

    logoTl.play()

}

function check_if_in_view() {

    var window_height = $window.height();
    var window_top_position = $window.scrollTop();
    var window_bottom_position = (window_top_position + window_height);
    $bgs = $('.bgs').find('.bg');
    bgGradientInBetween = false;

    $.each($animation_elements, function(i, el) {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);

        // if the item is above the half of the viewport
        if ( element_top_position < ( window_bottom_position - (1 * (window_height/2) ) ) || (isXs && i == 0) ) {

            // actions
            var video = $element.find('video');
            if(video.length) {
                video.get(0).play();
            }

            var $card = $element.find('.card-anim');

            if( ! $card.hasClass('reveal') ) {
                $card.addClass('reveal')

                var $cardInners = $element.find('.card-anim .anim');

                revealTl = new TimelineMax();
                revealTl
                .set($card, {
                  opacity: 0,
                  y: 20
                })
                .set($cardInners, {
                  opacity: 0,
                  x: -10
                })
                .staggerTo($card, 0.5, {
                  opacity: 1,
                  y: 0,
                  clearProps:"transform",
                  ease:Power3.easeInOut
                  , onStart:tweenComplete, onStartParams:["{self}"]
                }, 0.2 )
                // .staggerFrom($cardInners, 0.5, {
                //       opacity: 0,
                //       x: -10,
                //       clearProps:"transform",
                //       ease:Power3.easeInOut
                //     }, 0.25, "-=0.3")

                function tweenComplete(tween) {
                  var card = tween.target;

                    var $oneCard = $(card);
                    var $cardInners = $oneCard.find('.anim');

                    innerRevealTl = new TimelineMax();
                    innerRevealTl
                    .staggerTo($cardInners, 0.4, {
                      opacity: 1,
                      x: 0,
                      clearProps:"transform",
                      ease:Power3.easeInOut
                    }, 0.05, "+=0.2")
                }

                $card.each(function(i, card) {


                });


            }
        }
    });

}
